using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ClassList
{
    public interface ICustomCollection<T>
    {
        /// <summary>
        /// Размер коллекции
        /// </summary>
        int Size();

        /// <summary>
        /// Признак пустого списка
        /// </summary>
        bool isEmpty();

        /// <summary>
        /// Имеется ли элемент в коллекции
        /// </summary>
        /// <param name="elem">Элемент, который ищем в коллекции</param>
        bool Contains(T elem);

        /// <summary>
        /// Добавление элемента в конец
        /// </summary>
        void Add(T elem);

        /// <summary>
        /// Добавление в конец нескольких элементов
        /// </summary>
        /// <param name="elems"></param>
        void AddRange(T[] elems);

        /// <summary>
        /// Удаление элемента со значением
        /// </summary>
        void Remove(T elem);

        /// <summary>
        /// Удаляет все элементы со значением
        /// </summary>
        void RemoveAll(T elem);

        /// <summary>
        /// Удаление элемента на позиции с номером 
        /// </summary>
        void RemoveAt(int index);

        /// <summary>
        /// Очищение коллекции
        /// </summary>
        void Clear();

        /// <summary>
        /// Переворачивает список
        /// </summary>
        void Reverse();

        /// <summary>
        /// Вставка элемента на конкретную позицию
        /// </summary>
        void Insert(int index, T elem);

        /// <summary>
        /// Возвращает позицию элемента
        /// </summary>
        int IndexOf(T elem);
    }
    public class LinkedNode<T> where T : IComparable<T>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T InfField;
        /// <summary>
        /// Следующий элемент
        /// </summary>
        public LinkedNode<T> NextNode;
        /// <summary>
        /// Предыдущий элемент
        /// </summary>
        public LinkedNode<T> PrevNode;
        public LinkedNode(T inf)
        {
            InfField = inf;
        }
    }
    public class CustomLinkedList<T> : IEnumerable where T : IComparable<T>
    {
        private LinkedNode<T> head;
        private LinkedNode<T> current;
        public CustomLinkedList() { }
        public CustomLinkedList(T elem)
        {
            head = new LinkedNode<T>(elem);
        }
        public CustomLinkedList(T[] array)
        {
            if (array == null && array.Length == 0)
                return;

            //создание головного элемента 
            head = new LinkedNode<T>(array[0]);

            if (array.Length > 1)
            {
                var headCopy = head;
                for (int i = 1; i < array.Length; i++)
                {
                    //создали новый узел (элемент связного списка)
                    var node = new LinkedNode<T>(array[i]);
                    //задаем новому узлу предыдущий элемент
                    //им будет тот элемент, на который смотрит headCopy
                    node.PrevNode = headCopy;
                    //последнему элементу списка присваиваем 
                    //ссылку на следующий элемент, которым
                    //является добавляемый элемент
                    headCopy.NextNode = node;
                    headCopy = headCopy.NextNode;
                }
            }
        }

        public override string ToString()
        {
            if (head == null)
                return "Список пуст";
            var sb = new StringBuilder();
            var headCopy = head;
            while (headCopy != null)
            {
                sb.Append(headCopy.InfField.ToString());
                if (headCopy.NextNode != null)
                    sb.Append("<=>");
                headCopy = headCopy.NextNode;
            }
            return sb.ToString();
        }

        public void WriteToConsole()
        {
            if (head == null) { Console.WriteLine("Empty"); return; }
            var headCopy = head;
            while (headCopy != null)
            {
                Console.Write(headCopy.InfField);
                Console.Write(" ");
                headCopy = headCopy.NextNode;
            }
            Console.WriteLine(" ");
        }

        public void Add(T elem)
        {
            if (head == null)
            {
                head = new LinkedNode<T>(elem);
                return;
            }
            var headCopy = head;
            while (headCopy.NextNode != null)
            {
                headCopy = headCopy.NextNode;
            }
            var newNode = new LinkedNode<T>(elem);
            headCopy.NextNode = newNode;
            newNode.PrevNode = headCopy;
        }

        public void AddRange(T[] elems)
        {
            if (elems == null && elems.Length == 0)
                return;
            foreach (var elem in elems)
            {
                this.Add(elem);
            }
        }


        public void Reverse()
        {
            if (head == null) { return; }
            var result = new CustomLinkedList<T>();
            LinkedNode<T> newHead = new LinkedNode<T>(head.InfField);
            var headCopy = head.NextNode;
            while (headCopy != null)
            {
                var nodeTemp = new LinkedNode<T>(headCopy.InfField);
                nodeTemp.NextNode = newHead;
                nodeTemp.PrevNode = null;
                newHead.PrevNode = nodeTemp;
                newHead = nodeTemp;
                headCopy = headCopy.NextNode;
            }
            head = newHead;
        }
        // Реализуем интерфейс IEnumerable
        public IEnumerator<T> GetEnumerator()
        {
            LinkedNode<T> current = head;
            while (current != null)
            {
                yield return current.InfField;
                current = current.NextNode;
            }
        }
        public void Reset()
        {
            current = head;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}